# Hello World

## Specifications

- Print the string of text "Hello, World!" to your [terminal](https://askubuntu.com/questions/38162/what-is-a-terminal-and-how-do-i-open-and-use-it).
- Once you have done this, [return 0](https://www.geeksforgeeks.org/return-statement-in-c-cpp-with-examples/) at the end of your [main()](https://opensource.com/article/19/5/how-write-good-c-main-function) function to exit with an error code of 0.

## Constraints

- Never copy and paste code from the internet; this is not only plagarism, but will not foster learning.

## Notes

- You may use the provided skeleton source code file.
- To print text in a C program:
    - Use [#include](https://www.techonthenet.com/c_language/directives/include.php) to include the Standard Input/Output ([stdio.h](https://www.tutorialspoint.com/c_standard_library/stdio_h.htm)) [library](https://en.wikibooks.org/wiki/C_Programming/Libraries) in your main.c source code file.
    - Use the function, Print Format ([printf()](https://www.tutorialspoint.com/c_standard_library/c_function_printf.htm)) from this library to print text to your terminal.
- 0 is generally synonymous with false, therefore, an error code of 0 implies that no error has occurred.
    - In the future, you may return alternative values to indicate certain error conditions in your programs.
- Programming is hard, but so long as you never give up, it will all make sense eventually!
