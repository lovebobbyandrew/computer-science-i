// Comments are indicated by a double forward slash, and are ignored by the compiler.
// The compiler is the program that turns this human-readable code into 1's and 0's for the computer!
// As shown below, you should always add your name and the date as comments to your code.
// The third line denotes the license I chose, but feel free to forgo it in code you write.

// Bobby Love
// October 21, 2022
// GNU GPLv3

// Compile this program with the following command: gcc main.c
// main.c is this source code file!
// Placing main.c after gcc causes main.c to become the target to be compiled.

// Execute this program with the following command: ./a.out
// a.out is the default name of the executable file created by gcc.
// Placing a filename directly after ./ will execute the file.

// The following statement includes a library, which allows for using pre-made functions in your code.
// This library, Standard Input/Output (stdio.h), allows for reading and printing from your terminal with functions such as scanf() and printf().
// If you forget to add a necessary library to your code, it will not compile, as the compiler won't know what functions you're trying to call!
// If you're curious and would like to see this, comment out the following #include statement, save, and then try to re-compile this file.

#include <stdio.h>

// main() is the first user defined function to execute upon running your program.
// The "int" before main() means that main() returns an integer (a whole, positive or negative number) upon completion.
// Data types such as int will be explored in depth later on.
// Any code located within the curly braces found directly after the parenthesis belongs to main().
// Compilers generally ignore whitespace, so you may see curly braces located on different lines in other people's code.
// They are equivalent, so long as the code statements found within the braces is left unchanged.

int main() {

    // Print Format (printf()) prints the text encapsulated within the quotes to the terminal.
    // '\n' is the newline character; without this character, the terminal would not jump to a new line upon printing the prior string of text.
    // Forgoing a newline at the end of the text string would result in outputting difficult to read text.
    // If you're curious about what this text would look like, try deleting the '\n' and then re-compiling and running this code.
    // Lastly, notice how the following printf() statement ends with a semicolon; this is required C syntax.
    // If you miss even a single semicolon, your code will not compile; if it does somehow compile, it will likely produce incorrect results.
    // Never forget your semicolons!

    printf("Hello, World!\n");

    // The following line of code returns the integer, 0, as mentioned in an above comment.
    // Such return values are often used as error codes, with 0 usually denoting no error, and any other number denoting an error of some kind.
    // In the future, you will learn how to error check your code (there will be no shortage of mistakes to be made, detected, and remedied).
    // This will allow for returning alternative values under various erroneous circumstances.
    // If you want to check the return code of your last executed program, use the following command: echo $?
    // To test this, try changing the return value to something other than 0, re-compiling and re-executing your program, and the reusing the aforementioned command.

    return 0;

}
