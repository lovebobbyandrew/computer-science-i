// Bobby Love
// October 7, 2022
// GNU GPLv3

// Build program with the following command: gcc -lm main.c -std=c11 -Wall -Werror
// Execute program with the following command: ./a.out

// stdbool.h allows for using the bool datatype.
#include <stdbool.h>
#include <stdio.h>
#include <math.h>

int main() {
    bool loop = true;
    int option = 0;
    float leg_one = 0.0;
    float leg_two = 0.0;
    float hypotenuse = 0.0;

    do {
        // puts() is a more efficient version of printf() for strings.
        // Use puts() when you don't need to print any variables.
        puts("================================================================");
        puts("Pythagorean Theorem");
        puts("================================================================");
        puts("(1): Find a Hypotenuse");
        puts("(2): Find a Leg");
        puts("(3): Quit");
        puts("================================================================");
        printf("Selection: ");
        scanf("%d", &option);

        // switch() constructs are used to make a decision based on the value of the variable argument.
        // Do know that switch() constructs can only accept integer constants as cases, however.
        // As a result, there are many cases where if () else trees are preferable.
        switch (option) {
            // if (option == 1) {
            case 1: {
                // Get the length of the first leg.
                do {
                    puts("================================================================");
                    printf("Leg One (1 <= Leg <= 100): ");
                    scanf("%f", &leg_one);
                // || is logical OR.
                // The following condtion will evaluate to true if leg_one is less than 1.0 OR greater than 100.0.
                } while (leg_one < 1.0 || leg_one > 100.0);

                // Get the length of the second leg.
                do {
                    puts("================================================================");
                    printf("Leg Two (1 <= Leg <= 100): ");
                    scanf("%f", & leg_two);
                } while (leg_two < 1.0 || leg_two > 100.0);

                // hypotenuse = ((leg_one ^ 2) + (leg_two ^ 2)) ^ (1 / 2)
                hypotenuse = sqrt(pow(leg_one, 2) + pow(leg_two, 2));
                puts("================================================================");
                printf("Hypotenuse: %.3f\n", hypotenuse);
                // Each case must end with a break, otherwise every successive case will execute.
                break;
            };

            case 2: {
                // Get the length of the unknown leg.
                do {
                    puts("================================================================");
                    printf("Known Leg (1 <= Leg <= 100): ");
                    scanf("%f", & leg_one);
                } while (leg_one < 1.0 || leg_one > 100.0);

                // Get the length of the hypotenuse.
                do {
                    puts("================================================================");
                    printf("Hypotenuse (Leg < Hypotenuse <= 150): ");
                    // %7f will read a float with up to 6 digits and a decimal point.
                    scanf("%7f", &hypotenuse);

                    // Clears the input buffer in case the previous user input is longer than permitted.
                    while ((getchar()) != '\n');
                // A hypotenuse may not be shorter than or equal to a leg.
                } while (hypotenuse <= leg_one || hypotenuse < 1.0 || hypotenuse > 150.0);

                leg_two = sqrt(pow(hypotenuse, 2) - pow(leg_one, 2));
                puts("================================================================");
                printf("Unknown Leg: %.3f\n", leg_two);
                break;
            };

            case 3: {
                puts("================================================================");
                puts("Farewell!");
                puts("================================================================");
                // This is called a sentinel variable.
                // Changing the value of this variable to false will cause us to break out of the outer do while () loop.
                loop = false;
                break;
            };

            // switch() constructs should contain every possible case.
            // The alternative to using a default case would be to write a case for every integer (integers are infinite, so that would be impossible).
            default: {
                puts("================================================================");
                puts("Invalid selection, try again.");
                // continue statements skip to the next iteration of the loop.
                // This continue statement isn't necessary, but increases code readability (very important to professionals).
                continue;
            };
        }
    } while (loop);
    return 0;
}
