# Pythagorean Theorem

## Specifications

- Print the following menu:
    - (1): Find a Hypotenuse
    - (2): Find a Leg
    - (3): Quit
- Read in the chosen option.
    - If 1 is selected:
        - Read in the lengths of the two legs.
            - 1 <= Length <= 100
        - Calculate and print the length of the hypotenuse.
        - Return to the menu.
    - If 2 is selected:
        - Read in the length of the known leg.
            - 1 <= Length <= 100
        - Read in the length of the hypotenuse.
            - 1 <= Length <= 150
        - Calculate and print the length of the unknown leg.
        - Return to the menu.
    - If 3 is selected:
        - Exit the program.
    - If an invalid option is selected:
        - Print an error message.
        - Return to the menu.

## Constraints

- Use a [do while()](https://www.tutorialspoint.com/cprogramming/c_do_while_loop.htm) loop for the main menu.

## Notes

- Use the [Pythagorean Theorem](https://en.wikipedia.org/wiki/Pythagorean_theorem) to find the missing side.
