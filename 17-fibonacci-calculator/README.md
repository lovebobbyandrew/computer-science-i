# Fibonacci Calculator

## Specifications

- Create a 1D array with 32 columns.
    - Initialize the indices to 0.
- Print the following menu:
    - (1): Iterative Fibonacci
    - (2): Recursive Fibonacci
    - (3): Print Array
    - (4): Reverse Array
    - (5): Exit
- Read in the chosen option.
    - If 1 is selected:
        - Print the execution time of the following:
            - Calculate and store the [fibonacci](https://www.mathsisfun.com/numbers/fibonacci-sequence.html) of each index of the array.
                - Fibonacci(0) -> array[0], Fibonacci(1) -> array[1]...
            - Perform the aforementioned task using iteration.
        - Return to the menu.
    - If 2 is selected:
        - Print the execution time of the following:
            - Calculate and store the factorial of each index of the array.
            - Perform the aforementioned task using tail recursion.
        - Return to the menu.
    - If 3 is selected:
        - Print the value of each index of the array.
        - Return to the menu.
    - If 4 is selected:
        - Reverse the contents of the array.
        - Return to the menu.
    - If 5 is selected:
        - Exit the program.
    - If an invalid option is selected:
        - Print an error message.
        - Return to the menu.

## Constraints

- Perform the aforementioned tasks using [pointer arithmetic](https://www.tutorialspoint.com/cprogramming/c_pointer_arithmetic.htm).

## Notes

- N/A
