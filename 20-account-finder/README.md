# Account Finder

## Specifications

- Using the [provided CSV file](problem/data.csv):
    - Find and print the details of the account with the highest balance.
    - Find and print the details of the account with the lowest balance.
    - Calculate and print the average account balance.
- Exit the program.

## Constraints

- Get the address of the CSV file using [command line arguments](https://www.tutorialspoint.com/cprogramming/c_command_line_arguments.htm).
- Define and use [struct](https://www.tutorialspoint.com/cprogramming/c_structures.htm) objects to organize account data from the CSV file.
- Open and read the file using [fopen()](https://www.tutorialspoint.com/c_standard_library/c_function_fopen.htm).
- When finished reading from the file, close the file with [fclose()](https://www.tutorialspoint.com/c_standard_library/c_function_fclose.htm).

## Notes

- CSV Format:
    - Name
    - Account Number
    - Balance
- If you fail to close a file you have opened, you will cause a [memory leak](https://en.wikipedia.org/wiki/Memory_leak).
    - Memory leaks are a hallmark of careless programming, so be sure to avoid them!
    - Memory usage can be monitored with [valgrind](https://valgrind.org/).
