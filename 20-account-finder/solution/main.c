// Bobby Love
// October 12, 2022
// GNU GPLv3

// Build program with the following command: gcc main.c -std=c11 -Wall -Werror -lm
// Execute program with the following command: ./a.out data.csv
// Check for memory leaks with the following command: valgrind ./a.out data.csv

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LEN 64

// This is called a structure, which is a custom, user defined data type that can hold primitive data types as members within.
// The following structure's type name is "struct account", and each instance holds: a char array of length LEN, an int, and a float.
struct account {
    char name[LEN];
    int ID;
    float balance;
};

struct account g_array[LEN];

char g_buffer[LEN];

int getLineCount(char*);

int getMaximum(int);

float getMean(int);

int getMinimum(int);

bool loadData(char*, int);

void printData(int);

void printLine(void);

void printStats(int);

int main(int argc, char **argv) {
    printLine();
    puts("Account Finder");
    if (argc != 2) {
        printLine();
        puts("Usage: ./a.out <Path to CSV File>");
        printLine();
        return 1;
    }
    // Opens the file with read-only access.
    FILE *data = fopen(argv[1], "r");
    if (!data) {
        printLine();
        puts("File is unopenable.");
        printLine();
        return 1;
    }
    int line_count = getLineCount(argv[1]);
    loadData(argv[1], line_count);
    fclose(data);
    printData(line_count);
    printStats(line_count);
    return 0;
}

int getLineCount(char *filename) {
    FILE *file = fopen(filename, "r");
    int line_count = 0;
    char curr = 0;
    // If the file is unopenable, there is no data to read.
    if (file) {
        // feof(file) returns true upon reaching the end of the argument file.
        while(!feof(file)) {
            curr = fgetc(file);
            if(curr == '\n') {
                ++line_count;
            }
        }
        // Always close files when finished reading or writing.
        fclose(file);
    }
    return line_count;
}

int getMaximum(int length) {
    int index = 0;
    float max = 0.0;
    for (int i = 0; i < length; ++i) {
        if (g_array[i].balance > max) {
            index = i;
            max = g_array[i].balance;
        }
    }
    return index;
}

float getMean(int length) {
    float sum = 0.0;
    float mean = 0.0;
    for (int i = 0; i < length; ++i) {
        // Members of a struct object are accessed with the period operator, as shown below.
        // In this example, the struct object is g_array[i], and the member being accessed is the float contained within, balance.
        sum += g_array[i].balance;
    }
    mean = sum / (float)length;
    return mean;
}

int getMinimum(int length) {
    int index = 0;
    float min = g_array[0].balance;
    for (int i = 0; i < length; ++i) {
        if (g_array[i].balance < min) {
            index = i;
            min = g_array[i].balance;
        }
    }
    return index;
}

bool loadData(char *filename, int line_count) { 
    FILE *file = fopen(filename, "r");
    bool result = false;
    // In case a file read failure occurs.
    if(!file) {
        result = true;
    } else {
        for (int i = 0; i < line_count; ++i) {
            // Improper input files cannot inject foreign code into the program.
            // They can cause improper output, however.
            fgets(g_buffer, LEN, file);
            // sscanf() (string scanf()) scans data from strings.
            // Scan data from g_buffer (a string) into the members of g_array[i], the struct object.
            sscanf(g_buffer, "%[^,],%d,%f\n", g_array[i].name, &(g_array[i].ID), &(g_array[i].balance));
        }
    }
    fclose(file);
    return result;
}

void printData(int line_count) {
    for (int i = 0; i < line_count; ++i) {
        printLine();
        printf("Name: %s\n", g_array[i].name);
        printf("ID: %d\n", g_array[i].ID);
        printf("Balance: $%.2f\n", g_array[i].balance);
    }
}

void printLine(void) {
    puts("================================================================");
}

void printStats(int line_count) {
    int max = 0;
    int min = 0;
    max = getMaximum(line_count);
    printLine();
    puts("Highest Balance");
    printf("Name: %s\n", g_array[max].name);
    printf("Account Number: %d\n", g_array[max].ID);
    printf("Balance: $%.2f\n", g_array[max].balance);
    min = getMinimum(line_count);
    printLine();
    puts("Lowest Balance");
    printf("Name: %s\n", g_array[min].name);
    printf("Account Number: %d\n", g_array[min].ID);
    printf("Balance: $%.2f\n", g_array[min].balance);
    printLine();
    printf("Mean balance: $%.2f\n", getMean(line_count));
    printLine();
}
