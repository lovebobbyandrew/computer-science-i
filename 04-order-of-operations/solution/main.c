// Bobby Love
// October 24, 2022
// GNU GPLv3

// Compile this program with the following command: gcc main.c
// Execute this program with the following command: ./a.out

// math.h allows for using the pow() function.
#include <math.h>
#include <stdio.h>

int main() {

    // Notice how printf() can accept expressions as printable arguments.
    // pow() returns a float, so cast the return value to an int.
    printf("2 ^ 4 + 6 * 3 - 7 = %d\n", (int)pow(2, 4) + 6 * 3 - 7);
    printf("2 ^ (4 + 6) * 3 - 7 = %d\n", (int)pow(2, 4 + 6) * 3 - 7);
    printf("2 ^ 4 + 6 * (3 - 7) = %d\n", (int)pow(2, 4) + 6 * (3 - 7));
    printf("2 ^ (4 + 6 * (3 - 7)) = %d\n", (int)pow(2, 4 + 6 * (3 - 7)));

    return 0;

}