// YOUR NAME HERE
// THE DATE HERE

// Compile this program with the following command: gcc main.c
// Execute this program with the following command: ./a.out

// math.h allows for using the pow() function.
#include <math.h>
#include <stdio.h>

int main() {

    // Here, print the resulting values of the listed expressions.
    // Note that printf() can accept expressions as printable arguments.
    // pow() returns a float, so cast the return value to an int to match data type of the other operands.
    // YOUR CODE HERE

    return 0;

}