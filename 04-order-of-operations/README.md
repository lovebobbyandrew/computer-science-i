# Order of Operations

## Specifications

- Calculate and print the result of the following expression:
    - 2 ^ 4 + 6 * 3 - 7
- Calculate and print the result of the following expression:
    - 2 ^ (4 + 6) * 3 - 7
- Calculate and print the result of the following expression:
    - 2 ^ 4 + 6 * (3 - 7)
- Calculate and print the result of the following expression:
    - 2 ^ (4 + 6 * (3 - 7))
- Exit the program.

## Constraints

- Use the Math library ([math.h](https://www.tutorialspoint.com/c_standard_library/math_h.htm)) for the [pow()](https://www.scaler.com/topics/pow-function-in-c/) function.
    - pow() allows for exponentiation; that is to say, the caret operator (^) is not used for exponentiation in C.

## Notes

- Order of operations is fundamental knowledge to programming.
    - If you use unsure of the order of operations, use parenthesis!
