# Column Analyzer

## Specifications

- Create a [2D array](https://www.tutorialspoint.com/cprogramming/c_multi_dimensional_arrays.htm) with 8 rows and 8 columns.
- Randomize the contents of the array.
    - 0 <= Element <= 9
- Print the value of each index of the array.
- Read in a desired column value.
    - 0 <= Column <= 4
- Find and print the minimum and maximum of the column.
- Calculate and print the mean of the column.

## Constraints

- N/A

## Notes

- [Nested for() loops](https://www.tutorialspoint.com/cprogramming/c_nested_loops.htm) are an excellent match for multi-dimensional arrays.
