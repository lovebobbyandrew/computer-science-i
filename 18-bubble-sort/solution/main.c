// Bobby Love
// October 11, 2022
// GNU GPLv3

// Build program with the following command: gcc main.c -std=c11 -Wall -Werror -lm
// Execute program with the following command: ./a.out

#include <time.h>
#include <stdio.h>
#include <stdlib.h>

#define SIZE 8

void bubbleSort(int[], int);

void fillArray(int[], int);

int getMaximum(int[], int);

float getMedian(int[], int);

int getMinimum(int[]);

void printArray(int[], int);

int main() {
    srand(time(NULL));
    int array[SIZE] = {0};
    puts("Bubble Sort");
    fillArray(array, SIZE);
    fputs("Unsorted Array: ", stdout);
    printArray(array, SIZE);
    bubbleSort(array, SIZE);
    fputs("Sorted Array: ", stdout);
    printArray(array, SIZE);
    printf("Minimum: %d\n", getMinimum(array));
    printf("Median: %.1f\n", getMedian(array, SIZE));
    printf("Maximum: %d\n", getMaximum(array, SIZE));
    puts("Farewell!");
    return 0;
}

// This is an inefficient method of sorting, but simple to understand.
void bubbleSort(int array[], int length) {
    int hold = 0;
    for (int i = 0; i < length; ++i) {
        for (int j = 0; j < length; ++j) {
            hold = array[i];
            if (array[i] < array[j]) {
                array[i] = array[j];
                array[j] = hold;
            }
        }
    }
}

void fillArray(int array[], int length) {
    for (int i = 0; i < length; ++i) {
        array[i] = rand() % 10;
    }
}

int getMaximum(int array[], int length) {
    return array[length - 1];
}

float getMedian(int array[], int length) {
    float median = 0.0;
    float sum = 0.0;
    if (length % 2) {
        median = (float)array[length / 2];
    } else {
        sum = (float)array[(length / 2) - 1] + (float)array[(length / 2)];
        median = sum / 2.0;
    }
    return median;
}

int getMinimum(int array[]) {
    return array[0];
}

void printArray(int array[], int length) {
    for (int i = 0; i < length; ++i) {
        printf("%d", array[i]);
        if (i == length - 1) {
            puts("");
        } else {
            fputs(" ", stdout);
        }
    }
}
