# Main Menu

## Specifications

- Print the following menu:
    - (1): Continue Loop
    - (2): Break Loop
- Read in the chosen option.
    - If 1 is selected:
        - Print a message.
        - Return to the menu.
    - If 2 is selected:
        - Print a message.
        - Exit the program.
    - If an invalid option is selected:
        - Print an error message.
        - Return to the menu.

## Constraints

- Use a [while() loop](https://www.tutorialspoint.com/cprogramming/c_while_loop.htm) or a [do while() loop](https://www.tutorialspoint.com/cprogramming/c_do_while_loop.htm) for this assignment.

## Notes

- while() type loops are best reserved for situations in which you will loop an indeterminant number of times.
    - This is in contrast to for() loops, which are most appropriate for when you know how many times you must loop.
