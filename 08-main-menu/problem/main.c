// YOUR NAME HERE
// THE DATE HERE

// Compile with program with the following command: gcc main.c
// Execute this program with the following command: ./a.out

// stdbool.h allows for the use of the bool (boolean) data type.
// A boolean is a variable that is either 1 (true) or 0 (false).
#include <stdbool.h>

#include <stdio.h>

int main() {

    int choice = 0;

    bool loop = true;

    while (loop == true) {

        // Print the menu options.
        // YOUR CODE HERE

        // Scan the user's choice into the variable, choice.
        // YOUR CODE HERE

        // Check the value of choice and and either: continue looping, break out of the loop, or print an error message.
        // YOUR CODE HERE

    }

    return 0;

}
