// Bobby Love
// October 23, 2022
// GNU GPLv3

// Compile with program with the following command: gcc main.c
// Execute this program with the following command: ./a.out

// stdbool.h allows for the use of the bool (boolean) data type.
// A boolean is a variable that is either 1 (true) or 0 (false).
#include <stdbool.h>

#include <stdio.h>

int main() {

    int choice = 0;

    bool loop = true;

    while (loop == true) {

        // Here, I print the menu options.
        printf("Main Menu\n");
        printf("(1): Continue Loop\n");
        printf("(2): Break Loop\n");
        printf("Selection: ");

        // Next, I scan in the user's choice with scanf().
        scanf("%d", &choice);

        // If this if() conditional evaluates to true, neither the else if() block nor the final else block will execute in the current iteration of the while() loop.
        if (choice == 1) {

            printf("Looping once more!\n");

        // If the first if() evaluates to false, this conditional will be checked.
        // If this if() conditional evalues to true, the final else block will not execute in this iteration of the loop.
        } else if (choice == 2) {

            printf("Breaking out of loop!\n");

            // When the while() conditional expression is checked, the boolean loop will no longer equal true, so the while() loop will not continue looping.
            loop = false;

        // This else block will only execute if both the first if() and the else if() both evaluate to false.
        // Note that this else block has the same effect as the if(choice == 1) block, other than the message that is printed.
        } else {

            // If neither 1 nor 2 was entered, then an invalid choice was entered.
            printf("Invalid option!\n");

        }

    }

    return 0;

}
