# Flight Reservation

## Specifications

- Create a [global](https://www.tutorialspoint.com/cprogramming/c_scope_rules.htm) 2D array with 12 rows and 6 columns.
    - Initialize every index in columns 1 and 4 to 200.
    - Initialize every index in columns 0, 2, 3, and 5 to 500.
    - This matrix represents the cost of each seat in each flight.
- Create three 2D arrays with 12 rows and 6 columns.
    - Each matrix represents a different flight.
- Using rand(), randomize the contents of the three matrices to:
    - X, for a taken seat.
    - O, for an open seat.
- Print the following menu:
    - (1): Administrator Login
    - (2): Reserve a Seat
    - (3): Quit
- Read in the chosen option.
    - If 1 is selected:
        - Read in a password.
            - Compare the password to a [hardcoded](https://en.wikipedia.org/wiki/Hard_coding) string of your choosing.
            - If the password is incorrect:
                - Return to the menu.
        - Print the seating status matrix for each flight.
        - Calculate and print each individual flight's revenue.
        - Calculate and print the total revenue.
        - Return to the menu.
    - If 2 is selected:
        - Read in the desired flight to book.
        - Print the matrix for the chosen flight.
        - Read in the row and column of the desired seat.
            - If the chosen seat is already taken:
                - Read in another row and another column.
        - Change the value of the chosen seat from an O to an X.
        - Return to the menu.
    - If 3 is selected:
        - Exit the program.
    - If an invalid option is selected:
        - Print an error message.
        - Return to the menu.

## Constraints

- N/A

## Notes

- N/A
