// Bobby Love
// October 10, 2022
// GNU GPLv3

// Build program with the following command: gcc main.c -std=c11 -Wall -Werror -lm
// Execute program with the following command: ./a.out

#include <stdio.h>
#include <stdlib.h>
// string.h allows for the use of strcmp() and other string related functions.
#include <string.h>
#include <time.h>

#define PASS_LEN 8
#define ROWS 12
#define COLS 6
#define MAX 25

// Global variable names are often prefixed with the letter g.
// They can be accessed anywhere by any function, which can be dangerous if abused.
// Not to mention, numerous security issues are made possible when global variables are used.
// With proper software design, global variables can (and should) generally be avoided.
// This array defines the cost of each seat in each plane.
int g_seat_cost[ROWS][COLS] = {
    {500, 200, 500, 500, 200, 500},
    {500, 200, 500, 500, 200, 500},
    {500, 200, 500, 500, 200, 500},
    {500, 200, 500, 500, 200, 500},
    {500, 200, 500, 500, 200, 500},
    {500, 200, 500, 500, 200, 500},
    {500, 200, 500, 500, 200, 500},
    {500, 200, 500, 500, 200, 500},
    {500, 200, 500, 500, 200, 500},
    {500, 200, 500, 500, 200, 500},
    {500, 200, 500, 500, 200, 500},
    {500, 200, 500, 500, 200, 500},
};

int getColumn(void);

int getFlight(void);

int getOption(void);

void getPassword(char[]);

int getRevenue(char[][COLS], int, int);

int getRow(void);

void getSeat(char[][COLS], int, int);

void initializeSeats(char[][COLS], int, int);

void printFlightMenu(void);

void printLine(void);

void printMenu(void);

void printSeats(char[][COLS], int, int);

int main() {
    // Seed srand() with time.
    srand(time(NULL));
    int option = 0;
    int flight = 0;
    int revenue = 0;
    int total_rev = 0;
    // Strings must end with a null termination character.
    // '\0' is the representation of null terminator.
    // Without a null terminator, the data is not a string, but rather just a character array.
    // Tip: "password" is a terrible password for real accounts!
    char password[PASS_LEN + 1] = {'p', 'a', 's', 's', 'w', 'o', 'r', 'd', '\0'};
    char input_pass[PASS_LEN + 2] = {0};
    char flight_one[ROWS][COLS] = {0};
    char flight_two[ROWS][COLS] = {0};
    char flight_three[ROWS][COLS] = {0};
    // Initialize the seating arrangements.
    initializeSeats(flight_one, ROWS, COLS);
    initializeSeats(flight_two, ROWS, COLS);
    initializeSeats(flight_three, ROWS, COLS);
    do {
        option = getOption();
        // The administrator dashboard.
        if (option == 1) {
            getPassword(input_pass);
            // The inputted password is correct.
            // strcmp() returns 0 if the two strings are the same.
            if (!strcmp(password, input_pass)) {
                // Flight #1 information.
                printLine();
                puts("Flight #1");
                printSeats(flight_one, ROWS, COLS);
                revenue = getRevenue(flight_one, ROWS, COLS);
                printLine();
                printf("Revenue: $%d\n", revenue);
                total_rev += revenue;
                // Flight #2 information.
                printLine();
                puts("Flight #2:");
                printSeats(flight_two, ROWS, COLS);
                revenue = getRevenue(flight_two, ROWS, COLS);
                printLine();
                printf("Revenue: $%d\n", revenue);
                total_rev += revenue;
                // Flight #3 information.
                printLine();
                puts("Flight #3");
                printSeats(flight_three, ROWS, COLS);
                revenue = getRevenue(flight_three, ROWS, COLS);
                printLine();
                printf("Revenue: $%d\n", revenue);
                total_rev += revenue;
                printLine();
                printf("Total Revenue: $%d\n", total_rev);
            // The inputted password is incorrect.
            } else {
                printLine();
                puts("Incorrect password, returning to menu.");
                // Restart do while() loop.
                continue;
            }
        // Seat reservation dashboard.
        } else if (option == 2) {
            flight = getFlight();
            if (flight == 1) {
                printSeats(flight_one, ROWS, COLS);
                getSeat(flight_one, ROWS, COLS);
            } else if (flight == 2) {
                printSeats(flight_two, ROWS, COLS);
                getSeat(flight_two, ROWS, COLS);
            // } if (flight == 3) {
            } else {
                printSeats(flight_three, ROWS, COLS);
                getSeat(flight_three, ROWS, COLS);
            }
        // Quits the program.
        // } if (option == 3) {
        } else {
            printLine();
            puts("Farewell!");
            printLine();
            break;
        }
    } while (1);
    return 0;
}

int getColumn(void) {
    int column = 0;
    long input_col = 0;
    // ** creates a pointer to a pointer (an address of an address).
    char **num_end = NULL;
    // Initialize all indices in the array to 0.
    char input_string[3] = {0};
    do {
        printLine();
        fputs("Column: ", stdout);
        // fgets() is a newer, safer alternative to scanf(), and should always be used for reading input.
        fgets(input_string, 3, stdin);
        // strtol() converts strings (NULL terminated char arrays) into long integers.
        input_col = strtol(input_string, num_end, 10);
        if (input_string[1] == '\n') {
            // Most users do not understand 0 indexed arrays, so offset the array by 1 to avoid confusion for the user.
            if (input_col > 0 && input_col < 7) {
                column = input_col - 1;
                break;
            } else {
                printLine();
                puts("Invalid column, try again.");
            }
        } else {
            while (getchar() != '\n');
            printLine();
            puts("Invalid column, try again.");
            continue;
        }
    } while (1);
    return column;
}

int getFlight(void) {
    long flight = 0;
    char **num_end = NULL;
    char input_string[3] = {0};
    do {
        printFlightMenu();
        fgets(input_string, 3, stdin);
        flight = strtol(input_string, num_end, 10);
        if (input_string[1] == '\n') {
            if (flight > 0 && flight < 4) {
                break;
            } else {
                printLine();
                puts("Invalid flight, try again.");
            }
        } else {
            while (getchar() != '\n');
            printLine();
            puts("Invalid flight, try again.");
            continue;
        }
    } while (1);
    return (int)flight;
}

int getOption(void) {
    long option = 0;
    char **num_end = NULL;
    char input_string[3] = {0};
    do {
        printMenu();
        fgets(input_string, 3, stdin);
        option = strtol(input_string, num_end, 10);
        if (input_string[1] == '\n') {
            if (option > 0 && option < 4) {
                break;
            } else {
                printLine();
                puts("Invalid selection, try again.");
            }
        } else {
            while (getchar() != '\n');
            printLine();
            puts("Invalid selection, try again.");
            continue;
        }
    } while (1);
    return option;
}

void getPassword(char input_pass[]) {
    // Ensures password string is clean from previous iterations.
    for (int i = 0; i < PASS_LEN + 2; ++i) {
        input_pass[i] = 0;
    }
    // fputs() is a faster alternative to printf().
    printLine();
    fputs("Password: ", stdout);
    // Can only get PASS_LEN character long passwords.
    fgets(input_pass, PASS_LEN + 2, stdin);
    if (input_pass[PASS_LEN] == '\n') {
        input_pass[PASS_LEN] = '\0';
    } else if (input_pass[PASS_LEN] != 0) {
        while (getchar() != '\n');
    }
}

int getRevenue(char flight[][COLS], int rows, int cols) {
    int revenue = 0;
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            // X means a seat is occupied, thus it will bring revenue.
            if (flight[i][j] == 'X') {
                if (j == 1 || j == 4) {
                    revenue += 200;
                } else revenue += 500;
            }
        }
    }
    return revenue;
}

int getRow(void) {
    long row = 0;
    char input_string[4] = {0};
    char **num_end = NULL;
    do {
        printLine();
        fputs("Row: ", stdout);
        fgets(input_string, 4, stdin);
        row = strtol(input_string, num_end, 10);
        if (input_string[1] == '\n' || input_string[2] == '\n') {
            if (row > 0 && row < 13) {
                --row;
                break;
            } else {
                printLine();
                puts("Invalid row, try again.");
            }
        } else {
            while (getchar() != '\n');
            printLine();
            puts("Invalid row, try again.");
            continue;
        }
    } while (1);
    return row;
}

void getSeat(char flight[][COLS], int rows, int cols) {
    int row = 0;
    int col = 0;
    do {
        row = getRow();
        col = getColumn();
        if (flight[row][col] == 'O') {
            flight[row][col] = 'X';
            printLine();
            puts("Seat reserved!");
            break;
        } else {
            printLine();
            puts("That seat is taken, try again.");
        }
    } while (1);
}

void initializeSeats(char seat_array[][COLS], int rows, int cols) {
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            // If rand() returns an odd number.
            if (rand() % 2) seat_array[i][j] = 'X';
            else seat_array[i][j] = 'O';
        }
    }
}

void printFlightMenu(void) {
    printLine();
    puts("(1): Flight #1");
    puts("(2): Flight #2");
    puts("(3): Flight #3");
    printLine();
    fputs("Flight: ", stdout);
}

void printLine(void) {
    puts("================================================================");
}

void printMenu(void) {
    printLine();
    puts("Flight Reservation");
    printLine();
    puts("(1): Administrator Login");
    puts("(2): Seat Reservation");
    puts("(3): Quit");
    printLine();
    fputs("Selection: ", stdout);
}

void printSeats(char seat_array[][COLS], int rows, int cols) {
    printLine();
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            printf("%c", seat_array[i][j]);
            if (j == cols - 1) printf("\n");
            else if (j == 2) printf("  ");
            else printf(" ");
        }
    }
}
