# Caesar Cipher

## Specifications

- Read in a string of letters.
    - 1 <= Length <= 8
- Generate a random number to be a [cipher key](https://en.wikipedia.org/wiki/Cipher).
    - 1 <= Key <= 25
- Perform a cipher operation on the string.
    - A cipher operation entails imposing an offset on each character.
    - Examples:
        - Key = 5
            - 'A' -> 'F'
            - 'R' -> 'W'
        - Key = 3
            - 'J' -> 'M'
            - 'Z' -> 'C'
        - Key = 9
            - 'A' -> 'J'
            - 'G' -> 'P'
- Print the ciphered string.
- Read in a guess of the key's value.
    - If the guess is incorrect:
        - Read in another guess of the cipher key's value.
    - If the guess is correct:
        - Exit the program.

## Constraints

- N/A

## Notes

- N/A
