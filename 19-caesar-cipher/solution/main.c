// Bobby Love
// October 11, 2022
// GNU GPLv3

// Build program with the following command: gcc main.c -std=c11 -Wall -Werror -lm
// Execute program with the following command: ./a.out

#include <ctype.h>
#include <stdbool.h>
#include <stdio.h> 
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define LEN 8

bool checkString(char[]);

int cipherString(char[], char[], int);

void clearStdin(void);

void getGuess(char[], char[], int);

void getString(char[], int);

int main() {
    srand(time(NULL));
    int key = 0;
    char unciphered[LEN] = {0};
    char ciphered[LEN] = {0};
    puts("Caesar Cipher");
    do {
        getString(unciphered, LEN);
    } while (checkString(unciphered));
    key = cipherString(unciphered, ciphered, LEN);
    getGuess(unciphered, ciphered, key);
    puts("Farewell!");
    return 0;
}

bool checkString(char string[]) {
    bool result = false;
    int i = 0;
    // Strings must end with a NULL terminator.
    while (string[i] != '\0') {
        // Valid strings contain only letters.
        // isalpha() returns true if the character argument is an alphabetic (a - z or A - Z) character.
        if (!isalpha(string[i])) {
            result = true;
        }
        ++i;
    }
    return result;
}

int cipherString(char unciphered[], char ciphered[], int length) {
    // This formula provides a range from 1 to 25.
    int key = rand() % 25 + 1;
    int difference = 0;
    int temp_key = 0;
    for (int i = 0; i < length; ++i) {
        // Allows for case insensitivity.
        // isupper() returns true if the character argument is an uppercase alphabetic (A - Z) character.
        if (isupper(unciphered[i])) {
            if (unciphered[i] + key <= 'Z') {
                ciphered[i] = unciphered[i] + key;
            } else {
                difference = 'Z' - unciphered[i];
                temp_key = key - difference - 1;
                ciphered[i] = 'A' + temp_key;
            }
        } else {
            if (unciphered[i] + key <= 'z') {
                ciphered[i] = unciphered[i] + key;
            } else {
                difference = 'z' - unciphered[i];
                temp_key = key - difference - 1;
                ciphered[i] = 'a' + temp_key;
            }
        }
        if (unciphered[i] == '\n') {
            ciphered[i] = '\0';
            break;
        }
    }
    return key;
}

void clearStdin(void) {
    while (getchar() != '\n');
}

void getGuess(char unciphered[], char ciphered[], int key) {
    int guess = 0;
    char *newline = NULL;
    char **num_end = NULL;
    char input[4] = {0};
    do {
        fputs("Unciphered String: ", stdout);
        puts(unciphered);
        fputs("Ciphered String: ", stdout);
        puts(ciphered);
        puts("What is the cipher key?");
        fputs("Answer: ", stdout);
        fgets(input, 4, stdin);
        // strchr() returns a pointer to the first occurrence of the query character in the given input string.
        newline = strchr(input, '\n');
        if (newline) {
            // strtol() conerts the string to a long int in the desired base (base 2, base 10, base 16...), if possible.
            guess = strtol(input, num_end, 10);
            if (guess == key) {
                puts("You are correct!");
                break;
            // If guess == 0, then an invalid input was entered.
            } else if (!guess) {
                puts("Invalid key value, try again.");
                continue;
            // If guess != 0, then an incorrect input was entered.
            } else {
                puts("Sorry, try again.");
                continue;
            }
        // If the input was too long, clear the input buffer.
        } else {
            while (getchar() != '\n');
            puts("Invalid key value, try again.");
            continue;
        }
    } while (1);
}

void getString(char string[], int length) {
    bool overflow;
    bool invalid;
    do {
        overflow = true;
        invalid = false;
        for (int i = 0; i < length; ++i) {
            string[i] = 0;
        }
        fputs("String: ", stdout);
        fgets(string, length, stdin);
        for (int i = 0; i < length; ++i) {
            if (string[i] == '\n') {
                overflow = false;
                break;
            }
        }
        if (overflow) {
            clearStdin();
        }
        for (int i = 0; i < LEN; ++i) {
            if (string[i] == '\n') {
                string[i] = '\0';
                break;
            }
            if (!isalpha(string[i])) {
                invalid = true;
                break;
            }
        }
        // strlen() returns the length of a given string.
        if (!strlen(string)) {
            invalid = true;
        }
        if (overflow || invalid) {
            puts("Invalid string, try again.");
        } else {
            break;
        }
    } while (1);
}
