# User Input

## Specifications

- Declare and intialize an input variable to 0.
    - Print the current value of the input variable.
- [Read in](https://www.tutorialspoint.com/c_standard_library/c_function_scanf.htm) an integer, and store that value into the input variable.
    - Print the new value of the input variable.
- Exit the program.

## Constraints

- N/A

## Notes

- N/A
