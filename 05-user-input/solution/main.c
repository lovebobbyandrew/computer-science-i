// Bobby Love
// October 22, 2022
// GNU GPLv3

// Compile this program with the following command: gcc main.c
// Execute this program with the following command: ./a.out

#include <stdio.h>

int main() {

    int input = 0;

    printf("Input an integer: ");

    // scanf() works much like printf(), but in reverse.
    // However, there is one key difference between the two.
    // When using scanf(), you must provide the memory address of the destination variable for the data.
    // This is done by adding an & to the front of the variable name, as seen below.
    // & returns the memory address of any variable it is attached to.
    // If you forget this & when using scanf(), then you will encounter a segmentation fault, which is when a program tries to access memory it does not own.
    scanf("%d", &input);

    printf("You chose: %d\n", input);

    return 0;

}
