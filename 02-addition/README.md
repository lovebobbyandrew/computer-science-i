# Addition

## Specifications

- Declare and initialize the following integer [variables](https://www.w3schools.com/c/c_variables.php).
    - int augend = 3;
    - int addend = 5;
    - int sum = 0;
    - Print the current values of these variables.
- Calculate and store the resulting sum of augend and addend into the variable, sum.
    - Print the new values of these variables.
- Exit the program.

## Constraints

- You must use variables (not [integer constants](https://www.oreilly.com/library/view/c-in-a/0596006977/ch03.html)) to perform the calculations.

## Notes

- Think of variables as containers to store data inside of.
    - Use variables of type [int](https://www.geeksforgeeks.org/int-1-sign-bit-31-data-bits-keyword-in-c/) to store the integers you will add together along with their sum.
    - The provided skeleton has already declared and initialized the variables for you, all you must do is use them.
- Use the + [operator](https://www.tutorialspoint.com/cprogramming/c_operators.htm) to add the [augend](https://en.wiktionary.org/wiki/augend) and [addend](https://en.wiktionary.org/wiki/addend) variables together.
- Use the = operator to assign the resulting value to the sum variable.
- To print a [base 10](https://www.thoughtco.com/definition-of-base-10-2312365) (decimal) integer, use the %d [format specifier](https://www.freecodecamp.org/news/format-specifiers-in-c/) inside of printf().
