// Bobby Love
// October 22, 2022
// GNU GPLv3

// Compile with program with the following command: gcc main.c
// Execute this program with the following command: ./a.out

// This include statments allows us to use the printf() function.
// Without this library, your code will not compile.
#include <stdio.h>

int main() {

    // The following lines of code are called variable declarations and initializations.
    // Declaration Example:
    // int number;
    // Initialization Example:
    // number = 5;
    // Declariation and Initialization Example:
    // int variable = 5;
    // You may declare and initialize a variable on different lines, but you must do both before you can use the variable.
    // If you try to use a variable that hasn't been declared, your code will not compile.
    // You should always initialize variables to some value, such as 0, even if you don't intend to use the value immediately.
    // Leaving variables uninitialized will likely cause bugs in the future, so try to avoid leaving them so.
    // augend, addend, and sum are all variables of the int data type, which means they can only hold integers (positive or negative whole numbers).
    int augend = 3;
    int addend = 5;
    int sum = 0;

    // Here, I am printing the values of the variables after initialization, but before I perform the addition operation on them.
    // The %d format specifier tells printf() to print a decimal integer; in this context, decimal means base 10, not decimal point.
    // The integer(s) to be printed by printf() are to be placed in a comma separated list after the string to be printed, as shown below.
    printf("Before Addition:\n");
    printf("augend = %d\n", augend);
    printf("addend = %d\n", addend);
    printf("sum = %d\n", sum);

    // Here, I am taking the values of variables augend and addend, adding those values together, and then assigning that value to the variable sum.
    sum = augend + addend;

    // A note regarding arithmetical operators in the C programming language:
    // = assigns the value of the right side expression to the variable on the left side.
    // + denotes addition.
    // - denotes subtraction.
    // * denotes multiplication.
    // / denotes division.
    // There are other operators, but those will not be necessary until later in your programming journey!

    // Here, I'm printing the values of the variables after I have performed the addition operation.
    // The values of augend and addend are unchanged, as they are on the right side of the = operator.
    // However, the value of the variable sum has changed to match the value of the expression: augend + addend
    // For clarification: augend + addend = 3 + 5 = 8
    printf("After Addition:\n");
    printf("augend = %d\n", augend);
    printf("addend = %d\n", addend);
    printf("sum = %d\n", sum);

    // Finally, as always with main(), I return an error code.
    // If the program has reached this line, then nothing bad has happened, so return 0 to indicate this.
    // 0 is synonymous with false, therefore, if the error code is false, then there was no error.
    return 0;

}
