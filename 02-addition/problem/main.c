// YOUR NAME HERE
// THE DATE HERE

// Compile with program with the following command: gcc main.c
// Execute this program with the following command: ./a.out

#include <stdio.h>

int main() {

    int augend = 3;
    int addend = 5;
    int sum = 0;

    printf("Before Addition:\n");
    // Print the values of the 3 variables from above.
    // YOUR CODE HERE

    // Perform addition by setting sum equal to the sum of augend and addend.
    // YOUR CODE HERE

    printf("After Addition:\n");
    // Print the values of the 3 variables once again.
    // You can reuse your code from the first time you printed them.
    // YOUR CODE HERE

    return 0;

}
