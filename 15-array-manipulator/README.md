# Array Manipulator

## Specifications

- Create a 2D array with 4 rows and 4 columns.
- Randomize the contents of the array.
    - 0 <= Element <= 9
- Print the value of each index of the array.
- Count and print the number of occurrences of 0.
- Separately print the even and odd values.
- Calculate and print the [transpose](https://support.microsoft.com/en-us/office/transpose-function-ed039415-ed8a-4a81-93e9-4b6dfac76027) of the array.
    - A transpose converts rows of the input array into columns of the output array.
- Calculate and print the 1D transformation of the transpose.

## Constraints

- N/A

## Notes

- N/A
