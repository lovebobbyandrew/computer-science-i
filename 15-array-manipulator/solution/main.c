// Bobby Love
// October 10, 2022
// GNU GPLv3

// Build program with the following command: gcc main.c -std=c11 -Wall -Werror -lm
// Execute program with the following command: ./a.out

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SIZE 4

int countZeroes(int[][SIZE], int, int);

// The first dimension of an array is not specified in function argument definitions.
// However, the size of all subsequent dimensions must be specified.
void fill2DArray(int[][SIZE], int, int);

// This function converts a 2D array into a 1D array.
void flattenArray(int[][SIZE], int, int, int[], int);

// This function modifies the input array into its transpose.
void getTranspose(int[][SIZE], int, int);

void printEvenOdd(int[][SIZE], int, int);

void print1DArray(int[], int);

void print2DArray(int[][SIZE], int, int);

int main() {
    srand(time(NULL));
    int array[SIZE][SIZE] = {0};
    int flat_array[SIZE * SIZE] = {0};
    puts("Array Manipulator");
    fill2DArray(array, SIZE, SIZE);
    print2DArray(array, SIZE, SIZE);
    printf("0 Count: %d\n", countZeroes(array, SIZE, SIZE));
    printEvenOdd(array, SIZE, SIZE);
    getTranspose(array, SIZE, SIZE);
    print2DArray(array, SIZE, SIZE);
    flattenArray(array, SIZE, SIZE, flat_array, SIZE * SIZE);
    print1DArray(flat_array, SIZE * SIZE);
    puts("Farewell!");
    return 0;
}

int countZeroes(int array[][SIZE], int rows, int cols) {
    int count = 0;
    puts("Counting zeroes...");
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            if (!array[i][j]) {
                ++count;
            }
        }
    }
    return count;
}

void fill2DArray(int array[][SIZE], int rows, int cols) {
    puts("Filling array...");
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            array[i][j] = rand() % 10;
        }
    }
}

void flattenArray(int array[][SIZE], int rows, int cols, int flat_array[], int length) {
    int flat_index = 0;
    puts("Flattening array...");
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            flat_array[flat_index] = array[i][j];
            ++flat_index;
        }
    }
}

void getTranspose(int array[][SIZE], int rows, int cols) {
    int transpose[SIZE][SIZE] = {0};
    puts("Finding transpose...");
    // Find the transpose and store it in a temporary array.
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            transpose[i][j] = array[j][i];
        }
    }
    // Replace the data in the input array with that of the temporary array.
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            array[i][j] = transpose[i][j];
        }
    }
}

void printEvenOdd(int array[][SIZE], int rows, int cols) {
    puts("Printing even values...");
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            // ! (logical NOT) inverts the value of the proceeding expression.
            // Example: !true == false
            // N % 2 returns 0 if N is even.
            if (!(array[i][j] % 2)) {
                printf("%d", array[i][j]);
            } else {
                printf(" ");
            }
            if (j == cols - 1) {
                printf("\n");
            } else {
                printf(" ");
            }
        }
    }
    puts("Printing odd values...");
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            if (array[i][j] % 2) {
                printf("%d", array[i][j]);
            } else {
                printf(" ");
            }
            if (j == cols - 1) {
                printf("\n");
            } else {
                printf(" ");
            }
        }
    }
}

void print1DArray(int array[], int length) {
    puts("Printing 1D array...");
    for (int i = 0; i < length; ++i) {
        printf("%d", array[i]);
        if (i == length - 1) {
            printf("\n");
        } else {
            printf(" ");
        }
    }
}

void print2DArray(int array[][SIZE], int rows, int cols) {
    puts("Printing 2D array...");
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            printf("%d", array[i][j]);
            if (j == cols - 1) {
                printf("\n");
            } else {
                printf(" ");
            }
        }
    }
}
