// Bobby Love
// October 23, 2022
// GNU GPLv3

// Compile this program with the following command: gcc main.c
// Execute this program with the following command: ./a.out

#include <stdio.h>

int main() {

    int input = 0;

    printf("Countdown: ");

    scanf("%d", &input);

    printf("Starting...\n");

    // This is called a for() loop.
    // for() loops are used when you know how many times you need to loop.
    // This for() loop:
    // 1. Sets i equal to the value of input.
    // 2. Checks if i is greater than 0.
    // 3. Executes the code within the braces.
    // 4. Decrements i by 1.
    // 5. Repeats steps 2 through 4 repeatedly until i is no longer greater than 0.
    for (int i = input; i > 0; --i) {

        printf("%d... ", i);

    }

    printf("Go!\n");

    return 0;

}
