# For Loops

## Specifications

- Read in an integer.
- Using a [for() loop](https://www.tutorialspoint.com/cprogramming/c_for_loop.htm), print every number ranging from the integer to 0.
- Exit the program

## Constraints

- N/A

## Notes

- for() loops are to be used when you know how many times you must loop.
    - This is in contrast to while() loops, which will loop an indeterminant number of times.
