// YOUR NAME HERE
// THE DATE HERE

// Compile this program with the following command: gcc main.c
// Execute this program with the following command: ./a.out

#include <stdio.h>

int main() {

    int input = 0;

    printf("Countdown: ");

    scanf("%d", &input);

    printf("Starting...\n");

    // Use a for() loop to count down from input.
    // This entails printing every value ranging from input to 0.
    // YOUR CODE HERE

    printf("Go!\n");

    return 0;

}
