# City Printer

## Specifications

- Using the [provided CSV file](problem/area.csv):
    - Load the location data from the file.
    - Assign each city a group based on its zip code:
        - Group 1:
            - 63000 <= Zip <= 63999
        - Group 2:
            - 64000 <= Zip <= 64999
        - Group 3:
            - 65000 <= Zip <= 65999
    - CSV Format:
        - City
        - County
        - Zip
- Print the following menu:
    - (1): Print CSV
    - (2): Print a City
    - (3): Print a County
    - (4): Print a Group
    - (5): Print a Zip
    - (6): Quit
- Read in the chosen option.
    - If 1 is selected:
        - Print all data from the file.
        - Return to the menu.
    - If 2 is selected:
        - Read in the desired city.
        - Print the city's information.
        - Return to the menu.
    - If 3 is selected:
        - Read in the desired county.
        - Print every city in the selected county.
        - Return to the menu.
    - If 4 is selected:
        - Read in the desired group.
        - Print every city of the selected group.
        - Return to the menu.
    - If 5 is selected:
        - Read in the desired zip code.
        - Print every city of the selected zip code.
        - Return to the menu.
    - If 6 is selected:
        - Exit the program.
    - If an invalid option is selected:
        - Print an error message.
        - Return to the menu.

## Constraints

- Use [malloc()](https://www.tutorialspoint.com/c_standard_library/c_function_malloc.htm) to create dynamically allocated arrays.
    - For every call to malloc(), a call to [free()](https://www.tutorialspoint.com/c_standard_library/c_function_free.htm) must be made to prevent [memory leaks](https://en.wikipedia.org/wiki/Memory_leak).
        - Never call free() on a [NULL](https://www.geeksforgeeks.org/few-bytes-on-null-pointer-in-c/) pointer, as the resulting behavior is [undefined](https://www.geeksforgeeks.org/undefined-behavior-c-cpp/).
        - Memory usage can be monitored with [valgrind](https://valgrind.org/).

## Notes

- Congratulations for making it this far!
