// Bobby Love
// October 12, 2022
// GNU GPLv3

// Build program with the following command: gcc main.c -std=c11 -Wall -Werror -lm
// Execute program with the following command: ./a.out area.csv
// Check for memory leaks with the following command: valgrind ./a.out area.csv

#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LEN 128

struct location {
    char city[LEN];
    char county[LEN];
    int zip;
    int group;
};

char g_buffer[LEN] = {0};

struct location* createLocationArray(int);

struct location* destroyLocationArray(struct location*);

int getLineCount(char*);

void groupAssign(struct location*, int);

int getGroup(void);

int getOption(void);

void getString(char*);

int getZip(void);

void loadFileData(char*, struct location*, int);

void printArray(struct location*, int);

void printCity(struct location*, int, char*);

void printCounty(struct location*, int, char*);

void printGroup(struct location*, int, int);

void printLine(void);

void printMenu(void);

void printZip(struct location*, int, int);

int main(int argc, char **argv) {
    int line_count = 0;
    int option = 0;
    // When declaring pointers, for the sake of consistency, the asterisk should always be on the variable name.
    // Example 1: int *a, *b, *c;
    // The above example declares three int pointer variables.
    // Example 2: int* a, *b, c;
    // The above example declares two int pointer variables and an int variable.
    // Example 3: int* a, b, c;
    // The above example declares an int pointer variable and two int variables.
    struct location *array = NULL;
    char city[LEN] = {0};
    char county[LEN] = {0};
    int group = 0;
    int zip = 0;
    if (argc != 2) {
        printLine();
        puts("Usage: ./a.out <Path to CSV File>");
        printLine();
        return 1;
    }
    FILE *file = fopen(argv[1], "r");
    if (!file) {
        printLine();
        puts("File is unopenable.");
        printLine();
        return 1;
    }
    // Unclosed files will appear as memory leaks if valgrind is invoked.
    fclose(file);
    line_count = getLineCount(argv[1]);
    array = createLocationArray(line_count);
    if (!array) {
        printLine();
        puts("malloc() has failed.");
        printLine();
        return 1;
    }
    loadFileData(argv[1], array, line_count);
    groupAssign(array, line_count);
    do {
        option = getOption();
        if (option == 1) {
            printArray(array, line_count);
        } else if (option == 2) {
            getString(city);
            printCity(array, line_count, city);
        } else if (option == 3) {
            getString(county);
            printCounty(array, line_count, county);
        } else if (option == 4) {
            group = getGroup();
            printGroup(array, line_count, group);
        } else if (option == 5) {
            zip = getZip();
            printZip(array, line_count, zip);
        } else {
            printLine();
            puts("Farewell!");
            printLine();
            break;
        }
    } while (1);
    array = destroyLocationArray(array);
    return 0;
}

void clearStdin(void) {
    while (getchar() != '\n');
}

struct location* createLocationArray(int length) {
    // malloc() allows for creating variables stored on the heap.
    // Heap memory is dynamic, as it can grow or shrink at runtime, as necessary.
    // That is to say, the size of arrays doesn't need be specified at compile time.
    // This can allow for versatile programs that use little memory by default, but can handle large datasets, if need be.
    // The downside of dynamic memory management is that for every call to malloc() a corresponding call to free() must be made (manually) later.
    // If you forget to call free() after using malloc(), this will result in a memory leak.
    // A memory leak occurs when a program reserves memory from the host operating system, but never gives it back.
    // This results in reduced performance and can causes crashes, if severe enough.
    // Never forget to invoke free() on dynamically allocted memory (memory you claimed via malloc())!
    struct location *array = malloc(sizeof(struct location) * length);
    // In case of malloc() failure.
    if (!array) {
        return NULL;
    }
    // Initialize member upon construction.
    for (int i = 0; i < length; ++i) {
        array[i].zip = 0;
        array[i].group = 0;
    }
    return array;
}

struct location* destroyLocationArray(struct location *array) {
    free(array);
    // Always set freed memory to NULL to prevent dangling pointers.
    return NULL;
}

int getLineCount(char *filename) {
    FILE *file = fopen(filename, "r");
    int line_count = 0;
    char curr = 0;
    // If the file is unopenable, there is no data to read.
    if (file) {
        while(!feof(file)) {
            curr = fgetc(file);
            if(curr == '\n') {
                ++line_count;
            }
        }
        // Always close files when finished reading or writing.
        fclose(file);
    }
    return line_count;
}

void groupAssign(struct location *array, int length) {
    for (int i = 0; i < length; ++i) {
        if (array[i].zip <= 63999) {
            array[i].group = 1;
        } else if (array[i].zip <= 64999) {
            array[i].group = 2;
        } else if (array[i].zip <= 65999) {
            array[i].group = 3;
        } else {
            array[i].group = 4;
        }
    }
}

void loadFileData(char *filename, struct location *array, int line_count) {
    FILE *file = fopen(filename, "r");
    for (int i = 0; i < line_count; ++i) {
        // Improper CSV files cannot inject foreign code into the program.
        // They can cause improper output, however.
        fgets(g_buffer, LEN, file);
        sscanf(g_buffer, "%[^,],%[^,],%d\n", array[i].city, array[i].county, &(array[i].zip));
    }
    fclose(file);
}

int getGroup(void) {
    char **end = NULL;
    int group = 0;
    bool overflow;
    do {
        overflow = true;
        // Clear global input buffer from any previous uses.
        for (int i = 0; i < LEN; ++i) {
            g_buffer[i] = 0;
        }
        printLine();
        fputs("Group: ", stdout);
        fgets(g_buffer, LEN, stdin);
        group = strtol(g_buffer, end, 10);
        for (int i = 0; i < LEN; ++i) {
            if (g_buffer[i] == '\n') {
                overflow = false;
                break;
            }
        }
        if (overflow) {
            clearStdin();
        }
        if (overflow || group < 1 || group > 4) {
            printLine();
            puts("Invalid group, try again.");
        } else {
            break;
        }
    } while (1);
    return group;
}

int getOption(void) {
    char **end = NULL;
    int option = 0;
    bool overflow;
    do {
        overflow = true;
        for (int i = 0; i < LEN; ++i) {
            g_buffer[i] = 0;
        }
        printMenu();
        fgets(g_buffer, LEN, stdin);
        option = strtol(g_buffer, end, 10);
        for (int i = 0; i < LEN; ++i) {
            if (g_buffer[i] == '\n') {
                overflow = false;
                break;
            }
        }
        if (overflow) {
            clearStdin();
        }
        if (overflow || option < 1 || option > 6) {
            printLine();
            puts("Invalid selection, try again.");
        } else {
            break;
        }
    } while (1);
    return option;
}

void getString(char *string) {
    bool overflow;
    bool invalid;
    do {
        overflow = true;
        invalid = false;
        for (int i = 0; i < LEN; ++i) {
            g_buffer[i] = 0;
        }
        printLine();
        fputs("Name: ", stdout);
        fgets(g_buffer, LEN, stdin);
        for (int i = 0; i < LEN; ++i) {
            if (g_buffer[i] == '\n') {
                overflow = false;
                break;
            }
        }
        if (overflow) {
            clearStdin();
        }
        for (int i = 0; i < LEN; ++i) {
            if (g_buffer[i] == '\n') {
                g_buffer[i] = '\0';
                break;
            }
            if (!isalpha(g_buffer[i]) && !isspace(g_buffer[i])) {
                invalid = true;
                break;
            }
        }
        if (overflow || invalid) {
            printLine();
            puts("Invalid selection, try again.");
        } else {
            break;
        }
    } while (1);
    strcpy(string, g_buffer);
}

int getZip(void) {
    char **end = NULL;
    int zip = 0;
    bool overflow;
    do {
        overflow = true;
        for (int i = 0; i < LEN; ++i) {
            g_buffer[i] = 0;
        }
        printLine();
        fputs("Zip: ", stdout);
        fgets(g_buffer, LEN, stdin);
        zip = strtol(g_buffer, end, 10);
        for (int i = 0; i < LEN; ++i) {
            if (g_buffer[i] == '\n') {
                overflow = false;
                break;
            }
        }
        if (overflow) {
            clearStdin();
        }
        if (overflow || zip < 1) {
            printLine();
            puts("Invalid selection, try again.");
        } else {
            break;
        }
    } while (1);
    return zip;
}

void printArray(struct location *array, int line_count) {
    for (int i = 0; i < line_count; ++i) {
        printLine();
        printf("CSV Line %d\n", i + 1);
        printf("City: %s\n", array[i].city);
        printf("County: %s County\n", array[i].county);
        printf("Zip: %d\n", array[i].zip);
        printf("Group: %d\n", array[i].group);
    }
}

void printCity(struct location *array, int line_count, char* city) {
    for (int i = 0; i < line_count; ++i) {
        if (!strcmp(array[i].city, city)) {
            printLine();
            printf("CSV Line %d\n", i + 1);
            printf("City: %s\n", array[i].city);
            printf("County: %s County\n", array[i].county);
            printf("Zip: %d\n", array[i].zip);
            printf("Group: %d\n", array[i].group);
        }
    }
}

void printCounty(struct location *array, int line_count, char* county) {
    for (int i = 0; i < line_count; ++i) {
        if (!strcmp(array[i].county, county)) {
            printLine();
            printf("CSV Line %d\n", i + 1);
            printf("City: %s\n", array[i].city);
            printf("County: %s County\n", array[i].county);
            printf("Zip: %d\n", array[i].zip);
            printf("Group: %d\n", array[i].group);
        }
    }
}

void printGroup(struct location* array, int line_count, int group) {
    for (int i = 0; i < line_count; ++i) {
        if (array[i].group == group) {
            printLine();
            printf("CSV Line %d\n", i + 1);
            printf("City: %s\n", array[i].city);
            printf("County: %s County\n", array[i].county);
            printf("Zip: %d\n", array[i].zip);
            printf("Group: %d\n", array[i].group);
        }
    }
}

void printLine(void) {
    puts("================================================================");
}

void printMenu(void) {
    printLine();
    puts("City Printer");
    printLine();
    puts("(1): Print CSV");
    puts("(2): Print a City");
    puts("(3): Print a County");
    puts("(4): Print a Group");
    puts("(5): Print a Zip");
    puts("(6): Quit");
    printLine();
    fputs("Selection: ", stdout);
}

void printZip(struct location* array, int line_count, int zip) {
    for (int i = 0; i < line_count; ++i) {
        if (array[i].zip == zip) {
            printLine();
            printf("CSV Line %d\n", i + 1);
            printf("City: %s\n", array[i].city);
            printf("County: %s County\n", array[i].county);
            printf("Zip: %d\n", array[i].zip);
            printf("Group: %d\n", array[i].group);
        }
    }
}
