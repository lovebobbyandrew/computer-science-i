// Bobby Love
// October 23, 2022
// GNU GPLv3

// Compile this program with the following command: gcc main.c
// Execute this program with the following command: ./a.out

#include <stdio.h>

int main() {

    int input = 0;

    printf("Type the number 5, and then press the enter key.\n");

    scanf("%d", &input);

    // This is called an if() statement.
    // The expression within the parenthesis is called the conditional expression.
    // If the expression evaluates to true, then the code within the curly braces will execute.
    // If the expression evaluates to false, then the code within the curly braces will not execute.
    if (input != 5) {

        printf("You did not type the number 5.\n");

        // A return code of 1 means the user did not follow the instructions.
        // Do understand that execution of any function, including main(), stops upon returning a value.
        // Thus, the code below this if() block will not execute if the following return 1 statement executes.
        // Check the return code of this program after execution with the following command: echo $?
        return 1;

    }

    // A note regarding conditional operators in the C programming language:
    // == returns true if the expressions on the left and right side of the operator are equivalent, otherwise it returns false.
    // != returns true if the expressions on the left and right side of the operator are not equivalent, otherwise it returns false.
    // > returns true if the expression on the left is greater than the expression on the right, otherwise it returns false.
    // >= returns true if the expression on the left is greater than or equal to the expression on the right, otherwise it return false.
    // < returns true if the expression on the left is less than the expression on the right, otherwise it returns false.
    // <= returns true if the expression on the left is less than or equal to the expression on the right, otherwise it returns false.
    // && is logical AND, which returns true if the expressions on the left and right side of the operator are true, otherwise it returns false.
    // || is logical OR, which returns true if either or both of the expressions on the left and right of the operator is true, otherwise it returns false.

    printf("You typed the number 5!\n");

    // A return code of 0 means the user followed the instructions.
    return 0;

}
