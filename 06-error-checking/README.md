# Error Checking

## Specifications

- Read in an integer.
- Compare the value of the input to 5.
    - If the input equals 5:
        - Print a message.
        - Exit the program by returning 0.
    - If the input does not equal 5:
        - Print an error message.
        - Exit the program by returning 1.

## Constraints

- N/A

## Notes

- An [if() else statement](https://www.tutorialspoint.com/cprogramming/if_statement_in_c.htm) would be appropriate for this problem.
