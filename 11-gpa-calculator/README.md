# GPA Calculator

## Specifications

- Read in the total number of courses taken.
    - Total >= 2
- Read in the grade percentage of each course.
    - 0 <= Percentage <= 100
- Read in the credit hour count of each course.
    - Credit(s) >= 1
- Calculate and print the [GPA](https://gpacalculator.net/college-gpa-calculator/), given the previous information.
- Print a comment about the GPA.

## Constraints

- N/A

## Notes

- Perform the aforementioned tasks using [functions](https://www.tutorialspoint.com/cprogramming/c_functions.htm).
    - A function should do one thing, and do it well!
