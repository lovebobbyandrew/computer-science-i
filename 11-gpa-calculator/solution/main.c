// Bobby Love
// October 9, 2022
// GNU GPLv3

// Build program with the following command: gcc main.c -std=c11 -Wall -Werror -lm
// Execute program with the following command: ./a.out

#include <stdbool.h>
#include <stdio.h>

// These are called function prototypes.
// They are necessary if a function used in main() is defined after main() or in another file.
// When a function is called in main(), the compiler scans for the definition of the function, and these protoypes ensure a definition exists.
// More advanced software design calls for using separate files for function prototypes and defintions, but that is beyond the scope of this course.
// Function prototypes should be located before main() if they are included at all.

// Ensures a student is taking at least 2 classes.
bool courseNumCheck(int);

// Gets the number of classes taken.
int getClassCount(void);

// Gets the credit hour count of a class.
int getClassCredits(int);

// Converts a grade weights into a GPA score.
float getGradePoint(int, int, int, int, int);

// Gets the grade percentage of a class.
int getPercentage(int);

// Ensures that a grade percentage is between 0 and 100.
bool percentCheck(int);

// Prints a comment about the final GPA.
void printComment(float);

// Ensures that a class is assigned a valid number of credit hours.
int unitErrorCheck(int);

int main() {
    int class_count = 0;
    int A = 0;
    int B = 0;
    int C = 0;
    int D = 0;
    int F = 0;
    int curr_credits = 0;
    int total_credits = 0;
    int curr_percentage = 0;
    float GPA;

    puts("GPA Calcuator");

    // The following function returns an int, which is then assigned to the variable class_count.
    class_count = getClassCount();

    do {
        curr_credits = getClassCredits(class_count);
        // totalUnits = totalUnits + currUnits;
        total_credits += curr_credits;
        curr_percentage = getPercentage(class_count);

        // Nested else if() constructs are useful for mutally exclusive conditions with various ranges.
        if (curr_percentage >= 90) {
            A += curr_credits;
        } else if (curr_percentage >= 80) {
            B += curr_credits;
        } else if (curr_percentage >= 70) {
            C += curr_credits;
        } else if (curr_percentage >= 60) {
            D += curr_credits;
        // if (curr_percentage < 60) {
        } else {
            F += curr_credits;
        }

        // -- is called the decrement operator, and in this case, is shorthand for the following:
        // class_count = class_count - 1;
        --class_count;
    // } while (class_count > 0);
    } while (class_count);

    printf("Total Credits: %d\n", total_credits);
    GPA = getGradePoint(A, B, C, D, F);
    printf("Overall GPA: %.2f\n", GPA);
    printComment(GPA);
    puts("Farewell!");

    return 0;
}

// These are called function definitions, are they are generally located after main(), if found in the same file.
// Note how each definition has a corresponding prototype above main().
bool courseNumCheck(int courses) {
    bool result = false;

    // Generally, returning true (or 1) means something has gone wrong in a function.
    if (courses < 2) {
        result = true;
    }

    return result;
}

int getClassCount(void) {
    int class_count = 0;

    do {
        printf("Enter the number of classes taken: ");
        scanf("%d", &class_count);
    // Note how a function is called as the argument of the do while() loop.
    } while (courseNumCheck(class_count));

    return class_count;
}

int getClassCredits(int class_number) {
    int units = 0;

    do {
        printf("How many credits is class %d: ", class_number);
        scanf("%d", &units);
    } while (unitErrorCheck(units));

    return units;
}

float getGradePoint(int A, int B, int C, int D, int F) {
    float GPA = 0.0;
    GPA += 4.0 * A;
    GPA += 3.0 * B;
    GPA += 2.0 * C;
    GPA += 1.0 * D;
    GPA += 0.0 * F;
    // GPA = GPA / (A + B + C + D + F);
    GPA /= A + B + C + D + F;
    return GPA;
}

int getPercentage(int class_number) {
    int percentage = 0;

    do {
        printf("What grade percentage was received in class %d: ", class_number);
        scanf("%d", &percentage);
    } while (percentCheck(percentage));

    return percentage;
}

bool percentCheck(int percentage) {
    bool result = false;

    // A grade percentage must be between 0 and 100.
    if (percentage > 100 || percentage < 0) {
        result = true;
    }

    return result;
}

void printComment(float GPA) {
    if (GPA >= 3.8) {
        puts("Comment: Outstanding!");
    } else if(GPA >= 3.5) {
        puts("Comment: Excellent!");
    } else if(GPA >= 3) {
        puts("Comment: Good!");
    } else if(GPA >= 2.5) {
        puts("Comment: Satisfactory!");
    } else if(GPA >= 2) {
        puts("Comment: Not satisfactory...");
    } else {
        puts("Comment: See you next semester!");
    }
}

int unitErrorCheck(int credits) {
    bool result = false;

    // A class must be worth at least 1 credit hour.
    if (credits < 1) {
        result = true;
    }

    return result;
}
