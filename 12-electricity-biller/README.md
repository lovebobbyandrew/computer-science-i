# Electricity Biller

## Specifications

- Print the following menu:
    - (1): Residential
    - (2): Commercial
    - (3): Industrial
    - (4): Quit
- Read in the chosen option.
    - If 1 is selected:
        - Read in the amount of power consumed in kWh.
        - Calculate and print the total bill based on the following rates:
            - Connection charge: $32.00
            - 0 <= kWh <= 200: $6.50/kWh
            - 200 < kWh <= 700: $12.25/kWh
            - 700 < kWh <= 1250: $16.50/kWh
            - kWh > 1250: $23.00/kWh
        - Return to the menu.
    - If 2 is selected:
        - Read in the amount of power consumed in kWh.
        - Calculate and print the total bill based on the following rates:
            - Connection charge: $99.99
            - 0 <= kWh <= 300: $13.50/kWh
            - 300 < kWh <= 1000: $17.00/kWh
            - 1000 < kWh <= 2000: $24.25/kWh
            - kWh > 2000: $30.50/kWh
        - Return to the menu.
    - If 3 is selected:
        - Read in the amount of power consumed in kWh.
        - Calculate and print the total bill based on the following rates:
            - Connection charge: $925.00
            - 0 <= kWh <= 500: $39.25/kWh
            - 500 < kWh <= 2000: $46.50/kWh
            - 2000 < kWh <= 3000: $55.50/kWh
            - kWh > 3000: $73.25/kWh
        - Return to the menu.
    - If 4 is selected:
        - Calculate and print the number of bills calculated.
        - Calculate and print the grand total of the bills.
        - Exit the program.
    - If an invalid option is selected:
        - Print an error message.
        - Return to the menu.

## Constraints

- N/A

## Notes

- N/A
