// Bobby Love
// October 10, 2022
// GNU GPLv3

// Build program with the following command: gcc main.c -std=c11 -Wall -Werror -lm
// Execute program with the following command: ./a.out

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

float getBill(int, int);

int getKiloWattHours(void);

int getOption(void);

void printLine(void);

void printMenu(void);

int main () {
    int option = 0;
    int kWh = 0;
    float curr_bill = 0.0;
    int bill = 0;
    float grand_total = 0.0;
    do {
        option = getOption();
        // If (4) is selected, the exit the loop.
        if (option == 4) {
            break;
        }
        kWh = getKiloWattHours();
        curr_bill = getBill(option, kWh);
        printLine();
        printf("Total: $%.2f\n", curr_bill);
        ++bill;
        grand_total += curr_bill;
    // while (1) implies an infinite loop that must be explicitly broken out of.
    } while (1);
    printLine();
    printf("Bills Calculated: %d\n", bill);
    printLine();
    printf("Grand Total: $%.2f\n", grand_total);
    printLine();
    puts("Farewell!");
    printLine();
    return 0;
}

float getBill(int option, int kWh) {
    float bill_amount = 0;
    if (option == 1) {
        // The connection charge is the same, regardless of the kWh consumed.
        bill_amount += 32.0;
        if (kWh <= 200) {
            bill_amount += kWh * 6.5;
        } else if (kWh <= 700) {
                bill_amount += kWh * 12.25;
        } else if (kWh <= 1250) {
                bill_amount += kWh * 16.5;
        } else {
                bill_amount += kWh * 23.0;
        }
    } else if (option == 2) {
        bill_amount += 99.99;
        if (kWh <= 300) {
            bill_amount += kWh * 13.5;
        } else if (kWh <= 1000) {
            bill_amount += kWh * 17.0;
        } else if (kWh <= 2000) {
            bill_amount += kWh * 24.25;
        } else {
            bill_amount += kWh * 30.5;
        }
    } else {
        bill_amount += 925.0;
        if (kWh <= 500) {
            bill_amount += kWh * 39.25;
        } else if (kWh <= 2000) {
            bill_amount += kWh * 46.5;
        } else if (kWh <= 3000) {
            bill_amount += kWh * 55.5;
        } else {
            bill_amount += kWh * 73.25;
        }
    }
    return bill_amount;
}

int getKiloWattHours(void) {
    int kWh = 0;
    do {
        printLine();
        fputs("kWh Consumed: ", stdout);
        scanf("%d", &kWh);
    } while (kWh < 0);
    return kWh;
}

int getOption(void) {
    int option = 0;
    do {
        printMenu();
        scanf("%d", &option);
    } while (option < 1 || option > 4);
    return option;
}

void printLine(void) {
    puts("================================================================");
}

void printMenu(void) {
    printLine();
    puts("Electricity Biller");
    printLine();
    puts("(1): Residential");
    puts("(2): Commercial");
    puts("(3): Industrial");
    puts("(4): Quit");
    printLine();
    fputs("Selection: ", stdout);
}
