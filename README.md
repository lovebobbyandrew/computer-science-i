# Computer Science I

## Preamble
This repository contains code assignments and examples similar to those I was assigned in my introductory university computer science class. Coding is perhaps the most rewarding activity I have ever done, and I encourage you to learn it too. Try to complete the following assignments and compare your solutions to mine; if you can successfully complete the following exercises, congratulations! You have equivalent knowledge of a university student who has completed an introductory programming course. Coding becomes easier with practice, so even if you initially find these problems difficult, keep trying! Your understanding will improve with time. Good luck!

## Assignments

### [01: Hello World](01-hello-world/README.md)

### [02: Addition](02-addition/README.md)

### [03: Data Types](03-data-types/README.md)

### [04: Order of Operations](04-order-of-operations/README.md)

### [05: User Input](05-user-input/README.md)

### [06: Error Checking](06-error-checking/README.md)

### [07: For Loops](07-for-loops/README.md)

### [08: Main Menu](08-main-menu/README.md)

### [09: Calculator](09-calculator/README.md)

### [10: Pythagorean Theorem](10-pythagorean-theorem/README.md)

### [11: GPA Calculator](11-gpa-calculator/README.md)

### [12: Electricity Biller](12-electricity-biller/README.md)

### [13: Array Analyzer](13-array-analyzer/README.md)

### [14: Column Analyzer](14-column-analyzer/README.md)

### [15: Array Manipulator](15-array-manipulator/README.md)

### [16: Flight Reservation](16-flight-reservation/README.md)

### [17: Fibonacci Calculator](17-fibonacci-calculator/README.md)

### [18: Bubble Sort](18-bubble-sort/README.md)

### [19: Caesar Cipher](19-caesar-cipher/README.md)

### [20: Account Finder](20-account-finder/README.md)

### [21: City Printer](21-city-printer/README.md)

## Environment
The code in this repository was written, ran, debugged, and published on a PC running [Arch Linux](https://archlinux.org/). Understanding how to operate UNIX-like systems is a worthwhile skill for the aspiring computer scientist, so I highly recommend trying a simple, user-friendly flavor of [Linux](https://en.wikipedia.org/wiki/Linux), such as [Ubuntu](https://ubuntu.com/), for the development of these projects. Both choices are free and open source, so you have nothing to lose, and much to gain! As for choice of IDE, I use and recommend [Visual Studio Code](https://code.visualstudio.com/), a powerful text editor available on all common Linux distributions.

## Credits
[Jim Ries](https://engineering.missouri.edu/faculty/jim-ries/) was the individual who taught me how to write code and prescribed the assignments that would become the basis of this repository. Passing his class wasn't easy, but the office hours he provided (during which he was immensely patient) were enlightening and allowed me to not only succeed, but also find the passion for programming I have today; I will never forget what he has done for me. In case you're reading this JimR: thank you for everything. In addition, many thanks to [Ekincan Ufuktepe](https://ekincanufuktepe.github.io/), [Jeffrey Uhlmann](https://en.wikipedia.org/wiki/Jeffrey_Uhlmann), [Mason Fleck](https://www.linkedin.com/in/masonrfleck/), and [Caden Bird](https://www.linkedin.com/in/cadenbird/), whose contributions and feedback proved invaluable to making this repository something I can be proud of.

## License
GNU GPLv3
