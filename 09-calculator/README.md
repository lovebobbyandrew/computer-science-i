# Simple Calculator

## Specifications

- Read in two [integers](https://www.tutorialspoint.com/cprogramming/c_data_types.htm).
    - If the second integer is not greater than 0:
        - Read in another integer.
- Calculate and [print](https://www.tutorialspoint.com/c_standard_library/c_function_printf.htm) the product, quotient, sum, and difference.
- Print whether the first integer [compared to](https://www.tutorialspoint.com/cprogramming/c_operators.htm) the second integer is:
    - Smaller
    - Equal
    - Larger
- Read in a third integer.
    - If the integer is not greater than -1:
        - Read in another integer.
- Calculate and print the [square root](https://www.programiz.com/c-programming/library-function/math.h/sqrt) of the third integer.
    - Print with [precision](https://www.geeksforgeeks.org/g-fact-41-setting-decimal-precision-in-c/) up to the thousandths place.
- [Exit](https://www.geeksforgeeks.org/return-statement-in-c-cpp-with-examples/) the program.

## Constraints

- N/A

## Notes

- To print a N digits left of the decimal point, use the following format specifier:
    - %.Nf
