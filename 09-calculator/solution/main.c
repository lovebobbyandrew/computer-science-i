// Bobby Love
// October 8, 2022
// GNU GPLv3

// Build program with the following command: gcc main.c -std=c11 -Wall -Werror -lm
// Execute program with the following command: ./a.out

// Always heed compiler messages!
// gcc is a C compiler; a complier converts human readable code into machine code.
// -lm invokes the linker, which allows for the use of functions in certain header files, such as math.h.
// main.c is the source code file to be compiled.
// -std=c11 specifies the standard of C to use (in this case, 2011); language features are added and deprecated in different versions.
// -Wall will print all compiler warnings upon compilation.
// -Werror will make code with warnings un-compilable; this is to help you avoid bad coding practices and simple mistakes.
// ./a.out executes the a.out file, which is the default name of a compiled executable.

// #include statements allow for the use of pre-defined library functions.
// <> tell the linker that the argument header file is located in the standard C header file location (used in this program).
// Example: #include <stdio.h>
// "" tell the linker that the arugment header file is loacted in the same folder as main.c, which is used for custom header files (not used in this program).
// Example: #include "custom.h"

// stdio.h allow for the use of I/O functions, such as printf() and scanf().
#include <stdio.h>
// math.h allows for use of the sqrt() function, among others.
#include <math.h>

// Using int before main() implies that main will return an integer upon completion.
// Return codes (usually found at the end of main()) are often reserved for error codes.
// Using no input parameter in the definition of main() implies that main() will not accept inputs upon the start of execution.
int main() {
    // It is good practice to declare variables near each other for the sake of readability.
    // Be sure to initialize integers to 0 or another value upon declaration (trust me, this will prevent bugs in the future).
    // The int datatype only allows for positive and negative whole numbers.
    int first_int = 0;
    int second_int = 0;
    int third_int = 0;
    int product = 0;
    // The float datatype allows for numbers to have value to the right of the decimal point.
    float quotient = 0.0;
    int difference = 0;
    int sum = 0;
    float root = 0.0;

    // printf() is a function used to print (formatted) text to stdout (standard output).
    // '\n' will print a newline when used as an argument to printf().
    printf("Simple Calculator\n");
    printf("Input an integer: ");

    // %d tells scanf() to read in a decimal integer.
    // scanf() requires an & before the destination variable.
    // This is because scanf() needs to know the destination memory address, and & returns the address of the attached variable.
    // This concept, pointers, will be discussed in later programs.
    scanf("%d", &first_int);

    // do while() loops will always execute once before checking the condition.
    // This means they can be slightly more efficient than regular while() loops.
    // That said, only use them if you know the loop needs to run at least once.
    // An alternative is the regular while() loop, which checks the condition every time (even the first).
    // Regular while() loops will almost always work, if you are unsure which type to use.
    // For the previous statement to hold true, you must always initialize varibles upon declaring them.
    do {
        printf("Input an integer greater than 0: ");
        scanf("%d", &second_int);
    // If the following condition evaluates to true, the loop will run again.
    } while (second_int < 1);

    // printf() can accept expressions and/or functions in place of variables as arguments.
    // I opted for the variable, product, for readability.
    product = first_int * second_int;
    printf("The product of the first and second integers is: %d\n", product);

    // The decimal result of the quotient will be truncated without the (float) casts on the int operands, as seen below.
    // Casting changes the data type of a variable, temporarily; be careful, however, as not all casts make sense.
    quotient = (float)first_int / (float)second_int;
    // %.3f will print a floating point number with exactly 3 digits to the right of the decimal point.
    printf("The quotient of the first and second integers is: %.3f\n", quotient);

    difference = first_int - second_int;
    printf("The difference of the first and second integers is: %d\n", difference);

    sum = first_int + second_int;
    printf("The sum of the first and second integers is: %d\n", sum);

    do {
        printf("Input an integer greater than -1: ");
        scanf("%d", &third_int);
    } while (third_int < 0);

    root = sqrt(third_int);
    printf("The square root of the third integer is: %.3f\n", root);

    // Generally, a return code of 0 implies no errors occurred.
    return 0;
}
