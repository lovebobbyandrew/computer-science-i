// YOUR NAME HERE
// THE DATE HERE

// Compile this program with the following command: gcc main.c
// Execute this program with the following command: ./a.out

#include <stdio.h>

int main() {

    // Declare and initialize your variables with the given parameters.
    // Variable names should be descriptive (for example, "x" is a bad variable name).
    // YOUR CODE HERE

    // This is an integer addition operation, as seen previously in the Addition assignment.
    printf("Integer Addition into an Integer Sum\n");
    // YOUR CODE HERE

    // This is a floating point addition operation, which is similar to integer addition, other than fractional values are allowed.
    printf("Floating Point Addition into a Float Sum\n");
    // YOUR CODE HERE

    // This is a floating point addition operation, whose sum is assigned to an integer.
    printf("Floating Point Addition into an Integer Sum\n");
    // YOUR CODE HERE

    // This is integer addition operation, whose sum is assinged to a float.
    printf("Integer Addition into a Float Sum\n");
    // To turn the float operand variables into int variables for the operation, type cast them!
    // YOUR CODE HERE

    // Don't forget to "return" an error code at the end of main().
    // YOUR CODE HERE

}
