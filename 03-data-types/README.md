# Data Types

## Specifications

- Add the values of i_aug and i_add together and store the resulting value into i_sum.
    - Print their values using printf().
- Add the values of f_aug and f_add together and store the resulting value into f_sum.
    - Print their values using printf().
- Add the values of f_aug and f_add together and store the resulting value into i_sum.
    - Print their values using printf().
- Add the values of f_aug and f_add together and store the resulting value into f_sum.
    - [Type cast](https://www.tutorialspoint.com/cprogramming/c_type_casting.htm) the values of f_aug and f_add to the int data type during the addition operation.
    - Print their values using printf().
- Exit the program.

## Constraints

- N/A

## Notes

- The results you find may not be what you are expecting, given the mixing of data types in this problem.
- Understanding how and when to type cast is a fundamental skill.
    - To type cast a variable of any data type into an int, use the cast operator as shown: (int)var_name
