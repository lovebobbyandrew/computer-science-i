// Bobby Love
// October 22, 2022
// GNU GPLv3

// Compile this program with the following command: gcc main.c
// Execute this program with the following command: ./a.out

#include <stdio.h>

int main() {

    // The int data type only supports integers (whole, positive or negative numbers).
    int i_aug = 5;
    int i_add = 3;
    int i_sum = 0;

    // The float data type only supports real numbers (whole or fractional, positive or negative numbers).
    float f_aug = 6.5;
    float f_add = 2.8;
    float f_sum = 0.0;

    // You may ask, what is the purpose of the int data type, as it cannot hold as many kinds of numbers as the float data type?
    // When choosing what data type to make a varible, think about the precision you will need for the data you intend to store.
    // If you need to keep the fractional part of your postive or negative numbers, then use float variables.
    // If you are only using whole, positive or negative numbers, then use int variables.
    // Do not use int variables to store fractional numbers or float variables to store exclusively whole numbers.
    // Data types are tools, and you should always use the most suitable tool for a given task.

    // This is an integer addition operation, as seen previously in the Addition assignment.
    printf("Integer Addition into an Integer Sum\n");
    i_sum = i_aug + i_add;
    printf("%d + %d = %d\n", i_aug, i_add, i_sum);

    // This is a floating point addition operation, which is similar to integer addition, other than fractional values are allowed.
    printf("Floating Point Addition into a Float Sum\n");
    f_sum = f_aug + f_add;
    // %f is the flag used for printing float variables.
    // Adding .1 between the % and f will cause only the whole number, decimal point, and tenths place to print.
    printf("%.1f + %.1f = %.1f\n", f_aug, f_add, f_sum);

    // This is a floating point addition operation, whose sum is assigned to an integer.
    // Notice how the fractional part of the answer is truncated when assigning the value to int_sum.
    // int_sum is an int, therefore, it cannot hold fractional values!
    printf("Floating Point Addition into an Integer Sum\n");
    i_sum = f_aug + f_add;
    printf("%.1f + %.1f = %d\n", f_aug, f_add, i_sum);

    // This is integer addition operation, whose sum is assinged to a float.
    // (int) temporarily turns the variable it is placed next to into an integer!
    // This is called type casting.
    // When I cast the augend and addend into int variables, their fractional parts were truncated.
    // Hence, despite storing their sum into a float variable, the fractional parts of the augend and addend were temporarily discarded during the operation.
    printf("Integer Addition into a Float Sum\n");
    f_sum = (int)f_aug + (int)f_add;
    printf("%d + %d = %.1f\n", (int)f_aug, (int)f_add, f_sum);

    return 0;

}
