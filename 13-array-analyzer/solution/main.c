// Bobby Love
// October 10, 2022
// GNU GPLv3

// Build program with the following command: gcc main.c -std=c11 -Wall -Werror -lm
// Execute program with the following command: ./a.out

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
// time.h allows for the use of time based functions, such as time().
#include <time.h>

// #define creates a constant value that is pasted in place by the compiler.
// In this case, any occurrence of "SIZE" will be replaced with 16.
// SIZE, as shown here, is a global constant and can be accessed anywhere by any function at any time.
// That is referred to as global scope.
// Hence, #define is useful for reoccurring numbers that are found in many scopes, but never change.
#define SIZE 16

void fillArray(int[], int);

float meanArray(int[], int);

void printArray(int[], int);

float stdDeviation(int[], int);

// Notice how most code is not found in main(), but rather the functions defined below main().
// Generally, good software design calls for main() to be used as a driver function to call other functions.
int main() {
    // Responsible for seeding the random number generator.
    srand(time(NULL));
    // Creates an array with SIZE indexes, all initialized to 0.
    int array[SIZE] = {0};
    puts("Array Analyzer");
    fillArray(array, SIZE);
    printArray(array, SIZE);
    printf("Mean: %.3f\n", meanArray(array, SIZE));
    printf("Standard Deviation: %.3f\n", stdDeviation(array, SIZE));
    puts("Farewell!");
    return 0;
}

void fillArray(int array[], int size) {
    for (int i = 0; i < size; ++i) {
        // % 10 ensures that any number rand() returns will be reduced to 9 or less.
        // array[i] is called array index notation, and is a means of accessing elements of an array.
        // In many languages, arrays are 0 indexed (the first index is accsessed as element 0).
        array[i] = rand() % 10;
    }
}

float meanArray(int array[], int size) {
    int sum = 0;
    float mean = 0.0;
    for (int i = 0; i < size; ++i) {
        sum += array[i];
    }
    // Cast the operands to floats to ensure no data is truncated.
    mean = (float)sum / (float)size;
    return mean;
}

void printArray(int array[], int size) {
    printf("Array: ");
    for (int i = 0; i < size; ++i) {
        printf("%d", array[i]);
        if (i != size - 1) {
            printf(" ");
        } else {
            printf("\n");
        }
    }
}

float stdDeviation(int array[], int size) {
    float stdDev = 0.0;
    float mean = meanArray(array, size);
    float summation = 0.0;
    float fraction = 0.0;
    for (int i = 0; i < size; ++i) {
        // pow(x,y) is equivalent to x^y as a caculator would display.
        summation += pow(array[i] - mean, 2);
    }
    fraction = summation / (float)size;
    stdDev = sqrt(fraction);
    return stdDev;
}
