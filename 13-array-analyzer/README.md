# Array Analyzer

## Specifications

- Create a [1D array](https://www.tutorialspoint.com/what-is-a-one-dimensional-array-in-c-language) with 16 indices.
    - Arrays in C are [0 indexed](https://en.wikipedia.org/wiki/Zero-based_numbering).
- Use [rand()](https://www.tutorialspoint.com/c_standard_library/c_function_rand.htm) to randomize the contents of the array.
    - 0 <= Element <= 9
- Calculate and print the [mean](https://en.wikipedia.org/wiki/Arithmetic_mean) of the array.
- Calculate and print the [standard deviation](https://www.scribbr.com/statistics/standard-deviation/) of the array.

## Constraints

- Use a [#define macro](https://www.programiz.com/c-programming/c-preprocessor-macros) to define the size of the array.

## Notes

- for() loops are excellent for traversing arrays.
